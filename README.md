# Compounds Viewer 

James Carter 2021-06-29

This is a webgl demo adapted to ES6 from an old
library called Glmol that visualises molecules
using various text based molecule formats.

A python script name augment_compounds.py in this directory was used
to augment the compounds.json data within src/compounds.json with the 
Protein Data Bank (pdb) format

## Quickstart

Open build/index.html in a Browser

or

    $ cd build

    $ npx serve

  or
    $ cd build

    $ python -m http.server
  
There is also the start of a Dockerfile

## npm start

To develop or use:

    $ npm install

    $ npm start

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## to test

   npm test




