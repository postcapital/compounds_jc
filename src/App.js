import MoleculeViewer from './Molecule.js';
import React, {Component} from 'react';
import compounds from './compounds.json';

class App extends Component { 

  onPrev () {
    var i = this.state.index - 1;
    if (i < 0) { 
      i = compounds.length-1;
    }
    this.setState({
      index: i,
      pdb: compounds[i].pdb,
      molecular_formula: compounds[i].molecular_formula
    });
  }
  
  onNext() {
    var i = this.state.index + 1;
    if (i > compounds.length-1) {
      i = 0;
    } 
    this.setState({
      index: i,
      pdb: compounds[i].pdb,
      molecular_formula: compounds[i].molecular_formula
    });
  }
  
  constructor(props) {
    super(props);
    this.state = { index: 0, pdb: compounds[0].pdb, molecular_formula: compounds[0].molecular_formula};
    this.onNext = this.onNext.bind(this);
    this.onPrev = this.onPrev.bind(this);
  }

  render() {
    return (
    <div>
      <div>
        <MoleculeViewer key={this.state.index}
                        pdb={this.state.pdb} />
      </div>
      <div>
        <div>Scroll to Zoom, Click to Rotate</div>
        <div><button onClick={this.onPrev}>P R E V </button></div>
        <div id="molecule_formula">{this.state.molecular_formula}</div>
        <div><button onClick={this.onNext}>N E X T </button></div>
      </div>
    </div>
    );
  }
}

export default App;

