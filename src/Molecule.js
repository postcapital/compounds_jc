/*
  Adapted to a React component by jc from GLmol.js
  https://github.com/biochem-fan/GLmol/blob/master/src/js/GLmol.js (C) Copyright 2011-2012, biochem_fan License: dual license of MIT or LGPL3
*/

import * as THREE from 'three/build/three.module.js';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import React, {Component} from 'react';

class MoleculeViewer extends Component {
  start = () => {
   if (!this.frameId) {
    this.frameId = requestAnimationFrame(this.animate)
   }
  }
  stop = () => {
   cancelAnimationFrame(this.frameId)
  }
  
  animate = () => {
   
   //this.rotationGroup.rotation.x += 0.05
   this.rotationGroup.rotation.y += 0.01
   this.rotationGroup.rotation.z += 0.01
   // moves into the screen and phades out
   this.renderScene()
   this.frameId = window.requestAnimationFrame(this.animate)
  }
  
  renderScene = () => {
   this.renderer.render(this.scene, this.camera)
  }
  
  render(){
   return(
    <div
      style={{ width: '800px', height: '500px' }}
      ref={(mount) => { this.mount = mount }}
    />
   )
  }
  componentDidUpdate() {
   this.componentDidMount();
  }
  componentDidMount(){
   this.key = this.props.key;
   const width = this.mount.clientWidth;
   const height = this.mount.clientHeight;  //ADD SCENE
   this.scene = new THREE.Scene()   //ADD CAMERA
   this.camera = new THREE.PerspectiveCamera(
    35,
    width/height,
    1,
    1000 
   )
   this.camera.position.set(0, 5, 20)
   //this.camera.position.z = 4   //ADD RENDERER
   this.renderer = new THREE.WebGLRenderer({ antialias: true });
   this.renderer.setClearColor('#222');
   this.renderer.setSize(width, height);
   this.mount.appendChild(this.renderer.domElement);
   this.setupMoleculeDrawing();
   this.scene.fog = new THREE.Fog(this.bgColor, 100, 200);
   this.modelGroup = new THREE.Object3D();
   this.rotationGroup = new THREE.Object3D();
   this.rotationGroup.add(this.modelGroup);
   this.scene.add(this.rotationGroup);
   var directionalLight =  new THREE.DirectionalLight(0xFFFFFF, 1.5);
   this.scene.add(directionalLight);
   var ambientLight = new THREE.AmbientLight(0xDDDDDD, 0.4);
   this.scene.add(ambientLight);
   this.loadMoleculeString(this.props.pdb);
   // this.setSlabAndFog();
   this.defineRepresentation();
   this.start();
   const controls = new OrbitControls(this.camera, this.mount);
   controls.target.set(0, 2, 0);

  }
  
  componentWillUnmount(){
   this.stop()
   this.mount.removeChild(this.renderer.domElement)
  }
  
  setupMoleculeDrawing() {
   
   this.ElementColors = {"H": 0xCCCCCC, "C": 0xAAAAAA, "O": 0xCC0000, "N": 0x0000CC, "S": 0xCCCC00, "P": 0x6622CC,
    "F": 0x00CC00, "CL": 0x00CC00, "BR": 0x882200, "I": 0x6600AA,
    "FE": 0xCC6600, "CA": 0x8888AA};
   this.Nucleotides = ['  G', '  A', '  T', '  C', '  U', ' DG', ' DA', ' DT', ' DC', ' DU'];
   
   // Reference: A. Bondi, J. Phys. Chem., 1964, 68, 441.
   this.vdwRadii = {"H": 1.2, "LI": 1.82, "NA": 2.27, "K": 2.75, "C": 1.7, "N": 1.55, "O": 1.52,
              "F": 1.47, "P": 1.80, "S": 1.80, "CL": 1.75, "BR": 1.85, "SE": 1.90,
              "ZN": 1.39, "CU": 1.4, "NI": 1.63};

   this.aaScale = 1;
   this.CAMERA_Z = -150;
   this.renderer.sortObjects = false; // hopefully improve performance
   this.fov = 20;
   this.fogStart = 0.4;
   this.slabNear = -50; // relative to the center of rotationGroup
   this.slabFar = +50;

    // Default values
   this.sphereRadius = 1.5;
   this.cylinderRadius = 0.4;
   this.lineWidth = 1.5 * this.aaScale;
   this.curveWidth = 3 * this.aaScale;
   this.defaultColor = 0xCCCCCC;
   this.sphereQuality = 16; //16;
   this.cylinderQuality = 16; //8;
   this.axisDIV = 5; // 3 still gives acceptable quality
   this.strandDIV = 6;
   this.nucleicAcidStrandDIV = 4;
   this.tubeDIV = 8;
   this.coilWidth = 0.3;
   this.helixSheetWidth = 1.3;
   this.nucleicAcidWidth = 0.8;
   this.thickness = 0.4;
  }

  parsePDB2(str) {
   console.log('parsing PDB2: ', str)
    var atoms = this.atoms;
    var protein = this.protein;
    var i, j;
    var n, m;
    // var atoms_cnt = 0;
    var lines = str.split("\n");
    for (i = 0; i < lines.length; i++) {
    var line = lines[i].replace(/^\s*/, ''); // remove indent
    var recordName = line.substr(0, 6);
    if (recordName === 'ATOM  ' || recordName === 'HETATM') {
        var atom, resn, chain, resi, x, y, z, hetflag, elem, serial, altLoc, b;
        altLoc = line.substr(16, 1);
        if (altLoc !== ' ' && altLoc !== 'A') continue; // FIXME: ad hoc
        serial = parseInt(line.substr(6, 5), 10);
        atom = line.substr(12, 4).replace(/ /g, "");
        resn = line.substr(17, 3);
        chain = line.substr(21, 1);
        resi = parseInt(line.substr(22, 5), 10);
        x = parseFloat(line.substr(30, 8));
        y = parseFloat(line.substr(38, 8));
        z = parseFloat(line.substr(46, 8));
        b = parseFloat(line.substr(60, 8));
        elem = line.substr(76, 2).replace(/ /g, "");
        if (elem === '') { // for some incorrect PDB files
        elem = line.substr(12, 4).replace(/ /g,"");
        }
        if (line[0] === 'H') hetflag = true;
        else hetflag = false;
        atoms[serial] = {'resn': resn, 'x': x, 'y': y, 'z': z, 'elem': elem.toUpperCase(),
  'hetflag': hetflag, 'chain': chain, 'resi': resi, 'serial': serial, 'atom': atom,
  'bonds': [], 'ss': 'c', 'color': 0xFFFFFF, 'bondOrder': [], 'b': b /*', altLoc': altLoc*/};
    } else if (recordName === 'SHEET ') {
        var startChain = line.substr(21, 1);
        var startResi = parseInt(line.substr(22, 4), 10);
        var endChain = line.substr(32, 1);
        var endResi = parseInt(line.substr(33, 4), 10);
        protein.sheet.push([startChain, startResi, endChain, endResi]);
     } else if (recordName === 'CONECT') {
// MEMO: We don't have to parse SSBOND, LINK because both are also
// described in CONECT. But what about 2JYT???
        var from = parseInt(line.substr(6, 5), 10);
        for (j = 0; j < 4; j++) {
        var to = parseInt(line.substr([11, 16, 21, 26][j], 5), 10);
        if (isNaN(to)) continue;
        if (atoms[from] !== undefined) {
            atoms[from].bonds.push(to);
            atoms[from].bondOrder.push(1);
        }
        }
     } else if (recordName === 'HELIX ') {
        var startChain = line.substr(19, 1);
        var startResi = parseInt(line.substr(21, 4), 10);
        var endChain = line.substr(31, 1);
        var endResi = parseInt(line.substr(33, 4), 10);
        protein.helix.push([startChain, startResi, endChain, endResi]);
     } else if (recordName === 'CRYST1') {
        protein.a = parseFloat(line.substr(6, 9));
        protein.b = parseFloat(line.substr(15, 9));
        protein.c = parseFloat(line.substr(24, 9));
        protein.alpha = parseFloat(line.substr(33, 7));
        protein.beta = parseFloat(line.substr(40, 7));
        protein.gamma = parseFloat(line.substr(47, 7));
        protein.spacegroup = line.substr(55, 11);
        this.defineCell();
    } else if (recordName === 'REMARK') {
        var type = parseInt(line.substr(7, 3), 10);
        if (type === 290 && line.substr(13, 5) === 'SMTRY') {
        n = parseInt(line[18], 10) - 1;
        m = parseInt(line.substr(21, 2), 10);
        if (protein.symMat[m] === undefined) protein.symMat[m] = new THREE.Matrix4().identity();
        protein.symMat[m].elements[n] = parseFloat(line.substr(24, 9));
        protein.symMat[m].elements[n + 4] = parseFloat(line.substr(34, 9));
        protein.symMat[m].elements[n + 8] = parseFloat(line.substr(44, 9));
        protein.symMat[m].elements[n + 12] = parseFloat(line.substr(54, 10));
        } else if (type === 350 && line.substr(13, 5) === 'BIOMT') {
        n = parseInt(line[18], 10) - 1;
        m = parseInt(line.substr(21, 2), 10);
        if (protein.biomtMatrices[m] === undefined) protein.biomtMatrices[m] = new THREE.Matrix4().identity();
        protein.biomtMatrices[m].elements[n] = parseFloat(line.substr(24, 9));
        protein.biomtMatrices[m].elements[n + 4] = parseFloat(line.substr(34, 9));
        protein.biomtMatrices[m].elements[n + 8] = parseFloat(line.substr(44, 9));
        protein.biomtMatrices[m].elements[n + 12] = parseFloat(line.substr(54, 10));
        } else if (type === 350 && line.substr(11, 11) === 'BIOMOLECULE') {
          protein.biomtMatrices = []; protein.biomtChains = '';
        } else if (type === 350 && line.substr(34, 6) === 'CHAINS') {
          protein.biomtChains += line.substr(41, 40);
        }
    } else if (recordName === 'HEADER') {
        protein.pdbID = line.substr(62, 4);
    } else if (recordName === 'TITLE ') {
        if (protein.title === undefined) protein.title = "";
        protein.title += line.substr(10, 70) + "\n"; // CHECK: why 60 is not enough???
    } else if (recordName === 'COMPND') {
           // TODO: Implement me!
    }
    }

    // Assign secondary structures
    for (i = 0; i < atoms.length; i++) {
    atom = atoms[i]; if (atom === undefined) continue;

    // MEMO: Can start chain and end chain differ?
    for (j = 0; j < protein.sheet.length; j++) {
        if (atom.chain !== protein.sheet[j][0]) continue;
        if (atom.resi < protein.sheet[j][1]) continue;
        if (atom.resi > protein.sheet[j][3]) continue;
        atom.ss = 's';
        if (atom.resi === protein.sheet[j][1]) atom.ssbegin = true;
        if (atom.resi === protein.sheet[j][3]) atom.ssend = true;
    }
    for (j = 0; j < protein.helix.length; j++) {
        if (atom.chain !== protein.helix[j][0]) continue;
        if (atom.resi < protein.helix[j][1]) continue;
        if (atom.resi > protein.helix[j][3]) continue;
        atom.ss = 'h';
        if (atom.resi === protein.helix[j][1]) atom.ssbegin = true;
        else if (atom.resi === protein.helix[j][3]) atom.ssend = true;
    }
    }
    protein.smallMolecule = false;
    return true;
};

  // Catmull-Rom subdivision
  subdivide(_points, DIV) { // points as Vector3
    var ret = [];
    var points;
    var i, j;
    points = []; // Smoothing test
    points.push(_points[0]);
    var lim;
    var p0, p1, p2, p3;
    for (i = 1, lim = _points.length - 1; i < lim; i++) {
     p1 = _points[i];
     p2 = _points[i + 1];
    if (p1.smoothen) points.push(new THREE.Vector3((p1.x + p2.x) / 2, (p1.y + p2.y) / 2, (p1.z + p2.z) / 2));
    else points.push(p1);
    }
    points.push(_points[_points.length - 1]);

    var size;
    for (i = -1, size = points.length; i <= size - 3; i++) {
    p0 = points[(i === -1) ? 0 : i];
    p1 = points[i + 1];
    p2 = points[i + 2];
    p3 = points[(i === size - 3) ? size - 1 : i + 3];
    var v0 = new THREE.Vector3().sub(p2, p0).multiplyScalar(0.5);
    var v1 = new THREE.Vector3().sub(p3, p1).multiplyScalar(0.5);
    for (j = 0; j < DIV; j++) {
        var t = 1.0 / DIV * j;
        var x = p1.x + t * v0.x
              + t * t * (-3 * p1.x + 3 * p2.x - 2 * v0.x - v1.x)
              + t * t * t * (2 * p1.x - 2 * p2.x + v0.x + v1.x);
        var y = p1.y + t * v0.y
              + t * t * (-3 * p1.y + 3 * p2.y - 2 * v0.y - v1.y)
              + t * t * t * (2 * p1.y - 2 * p2.y + v0.y + v1.y);
        var z = p1.z + t * v0.z
              + t * t * (-3 * p1.z + 3 * p2.z - 2 * v0.z - v1.z)
              + t * t * t * (2 * p1.z - 2 * p2.z + v0.z + v1.z);
        ret.push(new THREE.Vector3(x, y, z));
    }
    }
    ret.push(points[points.length - 1]);
    return ret;
};

  drawAtomsAsSphere(group, atomlist, defaultRadius, forceDefault, scale) {
    var sphereGeometry = new THREE.SphereGeometry(1, this.sphereQuality, this.sphereQuality); // r, seg, ring
    var i;
    for (i = 0; i < atomlist.length; i++) {
    var atom = this.atoms[atomlist[i]];
    if (atom === undefined) continue;

    var sphereMaterial = new THREE.MeshLambertMaterial({color: atom.color});
    var sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
    group.add(sphere);
    var r = (!forceDefault && this.vdwRadii[atom.elem] !== undefined) ? this.vdwRadii[atom.elem] : defaultRadius;
    if (!forceDefault && scale) r *= scale;
    sphere.scale.x = sphere.scale.y = sphere.scale.z = r;
    sphere.position.x = atom.x;
    sphere.position.y = atom.y;
    sphere.position.z = atom.z;
    }
};

  // about two times faster than sphere when div = 2
  drawAtomsAsIcosahedron(group, atomlist, defaultRadius, forceDefault) {
    var geo = this.IcosahedronGeometry();
    var i;
    for (i = 0; i < atomlist.length; i++) {
    var atom = this.atoms[atomlist[i]];
    if (atom === undefined) continue;

    var mat = new THREE.MeshLambertMaterial({color: atom.color});
    var sphere = new THREE.Mesh(geo, mat);
    sphere.scale.x = sphere.scale.y = sphere.scale.z = (!forceDefault && this.vdwRadii[atom.elem] !== undefined) ? this.vdwRadii[atom.elem] : defaultRadius;
    group.add(sphere);
    sphere.position.x = atom.x;
    sphere.position.y = atom.y;
    sphere.position.z = atom.z;
    }
};

  isConnected(atom1, atom2) {
    var s = atom1.bonds.indexOf(atom2.serial);
    if (s !== -1) return atom1.bondOrder[s];

    if (this.protein.smallMolecule && (atom1.hetflag || atom2.hetflag)) return 0; // CHECK: or should I ?

    var distSquared = (atom1.x - atom2.x) * (atom1.x - atom2.x) +
                 (atom1.y - atom2.y) * (atom1.y - atom2.y) +
                 (atom1.z - atom2.z) * (atom1.z - atom2.z);

//    if (atom1.altLoc !== atom2.altLoc) return false;
    if (isNaN(distSquared)) return 0;
    if (distSquared < 0.5) return 0; // maybe duplicate position.

    if (distSquared > 1.3 && (atom1.elem === 'H' || atom2.elem === 'H' || atom1.elem === 'D' || atom2.elem === 'D')) return 0;
    if (distSquared < 3.42 && (atom1.elem === 'S' || atom2.elem === 'S')) return 1;
    if (distSquared > 2.78) return 0;
    return 1;
};

  defineCell() {
    var p = this.protein;
    if (p.a === undefined) return;

    p.ax = p.a;
    p.ay = 0;
    p.az = 0;
    p.bx = p.b * Math.cos(Math.PI / 180.0 * p.gamma);
    p.by = p.b * Math.sin(Math.PI / 180.0 * p.gamma);
    p.bz = 0;
    p.cx = p.c * Math.cos(Math.PI / 180.0 * p.beta);
    p.cy = p.c * (Math.cos(Math.PI / 180.0 * p.alpha) -
            Math.cos(Math.PI / 180.0 * p.gamma)
          * Math.cos(Math.PI / 180.0 * p.beta))
          / Math.sin(Math.PI / 180.0 * p.gamma);
    p.cz = Math.sqrt(p.c * p.c * Math.sin(Math.PI / 180.0 * p.beta)
            * Math.sin(Math.PI / 180.0 * p.beta) - p.cy * p.cy);
};

  drawUnitcell(group) {
    var p = this.protein;
    if (p.a === undefined) return;
    var i;

    var vertices = [[0, 0, 0], [p.ax, p.ay, p.az], [p.bx, p.by, p.bz], [p.ax + p.bx, p.ay + p.by, p.az + p.bz],
        [p.cx, p.cy, p.cz], [p.cx + p.ax, p.cy + p.ay,  p.cz + p.az], [p.cx + p.bx, p.cy + p.by, p.cz + p.bz], [p.cx + p.ax + p.bx, p.cy + p.ay + p.by, p.cz + p.az + p.bz]];
    var edges = [0, 1, 0, 2, 1, 3, 2, 3, 4, 5, 4, 6, 5, 7, 6, 7, 0, 4, 1, 5, 2, 6, 3, 7];

    var geo = new THREE.SphereGeometry();
    for (i = 0; i < edges.length; i++) {
    geo.vertices.push(new THREE.Vector3(vertices[edges[i]][0], vertices[edges[i]][1], vertices[edges[i]][2]));
    }
    var lineMaterial = new THREE.LineBasicMaterial({linewidth: 1, color: 0xcccccc});
    var line = new THREE.Line(geo, lineMaterial);
    line.type = THREE.LinePieces;
    group.add(line);
};

// TODO: Find inner side of a ring
  calcBondDelta(atom1, atom2, sep) {
    var i;
    var dot;
    var delta;
    var axis = new THREE.Vector3(atom1.x - atom2.x, atom1.y - atom2.y, atom1.z - atom2.z).normalize();
    var found = null;
    for (i = 0; i < atom1.bonds.length && !found; i++) {
    var atom = this.atoms[atom1.bonds[i]]; if (!atom) continue;
    if (atom.serial !== atom2.serial && atom.elem !== 'H') found = atom;
    }
    for (i = 0; i < atom2.bonds.length && !found; i++) {
    var atom = this.atoms[atom2.bonds[i]]; if (!atom) continue;
    if (atom.serial !== atom1.serial && atom.elem !== 'H') found = atom;
    }
    if (found) {
    var tmp = new THREE.Vector3(atom1.x - found.x, atom1.y - found.y, atom1.z - found.z).normalize();
    dot = tmp.dot(axis);
    delta = new THREE.Vector3(tmp.x - axis.x * dot, tmp.y - axis.y * dot, tmp.z - axis.z * dot);
    }
    if (!found || Math.abs(dot - 1) < 0.001 || Math.abs(dot + 1) < 0.001) {
    if (axis.x < 0.01 && axis.y < 0.01) {
        delta = new THREE.Vector3(0, -axis.z, axis.y);
    } else {
        delta = new THREE.Vector3(-axis.y, axis.x, 0);
    }
    }
    delta.normalize().multiplyScalar(sep);
    return delta;
  }

  drawSmoothCurve(group, _points, width, colors, div) {
   if (_points.length === 0) return;
   var i;

   div = (div === undefined) ? 5 : div;
   var geo = new THREE.BufferGeometry();
   var points = this.subdivide(_points, div);

   for (i = 0; i < points.length; i++) {
    geo.vertices.push(points[i]);
    geo.colors.push(new THREE.Color(colors[(i === 0) ? 0 : Math.round((i - 1) / div)]));
   }
   var lineMaterial = new THREE.LineBasicMaterial({linewidth: width});
   lineMaterial.vertexColors = true;
   var line = new THREE.Line(geo, lineMaterial);
   line.type = THREE.LineStrip;
   group.add(line);
  } 

  drawAsCross(group, atomlist, delta) {
    var geo = new THREE.BufferGeometry();
    var points = [[delta, 0, 0], [-delta, 0, 0], [0, delta, 0], [0, -delta, 0], [0, 0, delta], [0, 0, -delta]];
    var i;
    var j;
    var lim;
    for (i = 0, lim = atomlist.length; i < lim; i++) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    var c = new THREE.Color(atom.color);
    for (j = 0; j < 6; j++) {
        geo.vertices.push(new THREE.Vector3(atom.x + points[j][0], atom.y + points[j][1], atom.z + points[j][2]));
        geo.colors.push(c);
    }
  }
  var lineMaterial = new THREE.LineBasicMaterial({linewidth: this.lineWidth});
  lineMaterial.vertexColors = true;
  var line = new THREE.Line(geo, lineMaterial, THREE.LinePieces);
  group.add(line);
};

  drawMainchainCurve(group, atomlist, curveWidth, atomName, div) {
    var points = [], colors = [];
    var currentChain, currentResi;
    if (div === undefined) div = 5;
    var i;

    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]];
    if (atom === undefined) continue;

    if ((atom.atom === atomName) && !atom.hetflag) {
        if (currentChain !== atom.chain || currentResi + 1 !== atom.resi) {
        this.drawSmoothCurve(group, points, curveWidth, colors, div);
        points = [];
        colors = [];
        }
        points.push(new THREE.Vector3(atom.x, atom.y, atom.z));
        colors.push(atom.color);
        currentChain = atom.chain;
        currentResi = atom.resi;
    }
    }
    this.drawSmoothCurve(group, points, curveWidth, colors, div);
};

  drawMainchainTube(group, atomlist, atomName, radius) {
    var points = [], colors = [], radii = [];
    var currentChain, currentResi;
    var i;
    for (i in atomlist) {
      var atom = this.atoms[atomlist[i]];
      if (atom === undefined) continue;

      if ((atom.atom === atomName) && !atom.hetflag) {
        if (currentChain !== atom.chain || currentResi + 1 !== atom.resi) {
          this.drawSmoothTube(group, points, colors, radii);
          points = []; colors = []; radii = [];
        }
        points.push(new THREE.Vector3(atom.x, atom.y, atom.z));
        if (radius === undefined) {
          radii.push((atom.b > 0) ? atom.b / 100 : 0.3);
        } else {
          radii.push(radius);
        }
        colors.push(atom.color);
        currentChain = atom.chain;
        currentResi = atom.resi;
      }
    }
    this.drawSmoothTube(group, points, colors, radii);
};

  drawStrip(group, p1, p2, colors, div, thickness) {
    console.log('drawStrip');
    if ((p1.length) < 2) return;
    div = div || this.axisDIV;
    p1 = this.subdivide(p1, div);
    p2 = this.subdivide(p2, div);
    if (!thickness) return this.drawThinStrip(group, p1, p2, colors, div);
    var i, j;

    var geo = new THREE.BufferGeometry();
    console.log("drawStrip:", geo.attributes);
    var vs = geo.getAttribute('position');
    var fs = geo.getAttribute('faces');
    var axis;
    var p1v;
    var p2v;
    var a1v;
    var a2v;
    var lim;
    for (i = 0, lim = p1.length; i < lim; i++) {
    vs.push(p1v = p1[i]); // 0
    vs.push(p1v); // 1
    vs.push(p2v = p2[i]); // 2
    vs.push(p2v); // 3
    if (i < lim - 1) {
        var toNext = p1[i + 1].clone().subSelf(p1[i]);
        var toSide = p2[i].clone().subSelf(p1[i]);
        axis = toSide.crossSelf(toNext).normalize().multiplyScalar(thickness);
    }
    vs.push(a1v = p1[i].clone().add(axis)); // 4
    vs.push(a1v); // 5
    vs.push(a2v = p2[i].clone().add(axis)); // 6
    vs.push(a2v); // 7
    }
    var faces = [[0, 2, -6, -8], [-4, -2, 6, 4], [7, 3, -5, -1], [-3, -7, 1, 5]];
    for (i = 1, lim = p1.length; i < lim; i++) {
    var offset = 8 * i, color = new THREE.Color(colors[Math.round((i - 1)/ div)]);
      for (j = 0; j < 4; j++) {
        var f = new THREE.Line(offset + faces[j][0], offset + faces[j][1], offset + faces[j][2], offset + faces[j][3], undefined, color);
        fs.push(f);
      }
    }
    var vsize = vs.length - 8; // Cap
    for (i = 0; i < 4; i++) {vs.push(vs[i * 2]); vs.push(vs[vsize + i * 2])};
    vsize += 8;
    fs.push(new THREE.Line(vsize, vsize + 2, vsize + 6, vsize + 4, undefined, fs[0].color));
    fs.push(new THREE.Line(vsize + 1, vsize + 5, vsize + 7, vsize + 3, undefined, fs[fs.length - 3].color));
    geo.computeFaceNormals();
    geo.computeVertexNormals(false);
    var material =  new THREE.MeshLambertMaterial();
    material.vertexColors = THREE.FaceColors;
    var mesh = new THREE.Mesh(geo, material);
    mesh.doubleSided = true;
    group.add(mesh);
  }

  IcosahedronGeometry() {
    if (!this.icosahedron) this.icosahedron = new THREE.IcosahedronGeometry(1);
    return this.icosahedron;
  }

  drawHelixAsCylinder(group, atomlist, radius) {
    var start = null;
    var currentChain, currentResi;

    var others = [], beta = [];
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]];
    if (atom === undefined || atom.hetflag) continue;
    if ((atom.ss !== 'h' && atom.ss !== 's') || atom.ssend || atom.ssbegin) others.push(atom.serial);
    if (atom.ss === 's') beta.push(atom.serial);
    if (atom.atom !== 'CA') continue;

    if (atom.ss === 'h' && atom.ssend) {
        if (start !== null) this.drawCylinder(group, new THREE.Vector3(start.x, start.y, start.z), new THREE.Vector3(atom.x, atom.y, atom.z), radius, atom.color, true);
        start = null;
    }
    currentChain = atom.chain;
    currentResi = atom.resi;
    if (start === null && atom.ss === 'h' && atom.ssbegin) start = atom;
    }
    if (start !== null) this.drawCylinder(group, new THREE.Vector3(start.x, start.y, start.z), new THREE.Vector3(atom.x, atom.y, atom.z), radius, atom.color);
    this.drawMainchainTube(group, others, "CA", 0.3);
    this.drawStrand(group, beta, undefined, undefined, true,  0, this.helixSheetWidth, false, this.thickness * 2);
  }

  drawCartoon(group, atomlist, doNotSmoothen, thickness) {
    this.drawStrand(group, atomlist, 2, undefined, true, undefined, undefined, doNotSmoothen, thickness);
  }

  drawStrand(group, atomlist, num, div, fill, coilWidth, helixSheetWidth, doNotSmoothen, thickness) {
    num = num || this.strandDIV;
    div = div || this.axisDIV;
    coilWidth = coilWidth || this.coilWidth;
    doNotSmoothen = (doNotSmoothen === undefined) ? false : true;
    helixSheetWidth = helixSheetWidth || this.helixSheetWidth;
    var i, j, k;
    var points = []; for (k = 0; k < num; k++) points[k] = [];
    var colors = [];
    var currentChain, currentResi, currentCA;
    var prevCO = null;
    var ss=null;
    var ssborder;

    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]];
    if (atom === undefined) continue;

    if ((atom.atom === 'O' || atom.atom === 'CA') && !atom.hetflag) {
        if (atom.atom === 'CA') {
        if (currentChain !== atom.chain || currentResi + 1 !== atom.resi) {
            for (j = 0; !thickness && j < num; j++)
              this.drawSmoothCurve(group, points[j], 1 ,colors, div);
            if (fill) this.drawStrip(group, points[0], points[num - 1], colors, div, thickness);
            var points = []; for (k = 0; k < num; k++) points[k] = [];
            colors = [];
            prevCO = null; 
            ss = null; 
            ssborder = false;
        }
        currentCA = new THREE.Vector3(atom.x, atom.y, atom.z);
        currentChain = atom.chain;
        currentResi = atom.resi;
        ss = atom.ss; ssborder = atom.ssstart || atom.ssend;
        colors.push(atom.color);
        } else { // O
        var O = new THREE.Vector3(atom.x, atom.y, atom.z);
        O.subSelf(currentCA);
        O.normalize(); // can be omitted for performance
        O.multiplyScalar((ss === 'c') ? coilWidth : helixSheetWidth);
        if (prevCO !== undefined && O.dot(prevCO) < 0) O.negate();
        prevCO = O;
        for (j = 0; j < num; j++) {
            var delta = -1 + 2 / (num - 1) * j;
            var v = new THREE.Vector3(currentCA.x + prevCO.x * delta,
                        currentCA.y + prevCO.y * delta, currentCA.z + prevCO.z * delta);
            if (!doNotSmoothen && ss === 's') v.smoothen = true;
            points[j].push(v);
        }
        }
    }
    }
    for (i = 0; !thickness && i < num; i++)
    this.drawSmoothCurve(group, points[i], 1 ,colors, div);
    if (fill) this.drawStrip(group, points[0], points[num - 1], colors, div, thickness);
  }

  drawDottedLines(group, points, color) {
    var geo = new THREE.BufferGeometry();
    var step = 0.3;
    var i, j, lim;

    for (i = 0, lim = Math.floor(points.length / 2); i < lim; i++) {
    var p1 = points[2 * i], p2 = points[2 * i + 1];
    var delta = p2.clone().subSelf(p1);
    var dist = delta.length();
    delta.normalize().multiplyScalar(step);
    var jlim =  Math.floor(dist / step);
    for (j = 0; j < jlim; j++) {
        var p = new THREE.Vector3(p1.x + delta.x * j, p1.y + delta.y * j, p1.z + delta.z * j);
        geo.vertices.push(p);
    }
    if (jlim % 2 === 1) geo.vertices.push(p2);
    }

    var mat = new THREE.LineBasicMaterial({'color': color.getHex()});
    mat.linewidth = 2;
    var line = new THREE.Line(geo, mat, THREE.LinePieces);
    group.add(line);
  }

  getAllAtoms() {
    var ret = [];
    var i;
    for (i in this.atoms) {
    ret.push(this.atoms[i].serial);
    }
    return ret;
  }

// Probably I can refactor using higher-order functions.
  getHetatms(atomlist) {
    var ret = [];
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    if (atom.hetflag) ret.push(atom.serial);
    }
    return ret;
  }

  removeSolvents(atomlist) {
    var ret = [];
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    if (atom.resn !== 'HOH') ret.push(atom.serial);
    }
    return ret;
};

  getProteins(atomlist) {
    var ret = [];
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    if (!atom.hetflag) ret.push(atom.serial);
    }
    return ret;
};

  excludeAtoms(atomlist, deleteList) {
    var ret = [];
    var blackList = new Object();
    var _i;
    for (_i in deleteList) blackList[deleteList[_i]] = true;

    for (_i in atomlist) {
    var i = atomlist[_i];

    if (!blackList[i]) ret.push(i);
    }
    return ret;
  }

  getSidechains(atomlist) {
    var ret = [];
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    if (atom.hetflag) continue;
    if (atom.atom === 'C' || atom.atom === 'O' || (atom.atom === 'N' && atom.resn !== "PRO")) continue;
    ret.push(atom.serial);
    }
    return ret;
  }

  getAtomsWithin(atomlist, extent) {
    var ret = [];
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    if (atom.x < extent[0][0] || atom.x > extent[1][0]) continue;
    if (atom.y < extent[0][1] || atom.y > extent[1][1]) continue;
    if (atom.z < extent[0][2] || atom.z > extent[1][2]) continue;
    ret.push(atom.serial);
    }
    return ret;
  }

  getExtent(atomlist) {
    var xmin;
    var ymin;
    var zmin = 9999;
    var xmax;
    var ymax;
    var zmax = -9999;
    var xsum;
    var ysum;
    var zsum;
    var cnt = 0;
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;
    cnt++;
    xsum += atom.x; ysum += atom.y; zsum += atom.z;

    xmin = (xmin < atom.x) ? xmin : atom.x;
    ymin = (ymin < atom.y) ? ymin : atom.y;
    zmin = (zmin < atom.z) ? zmin : atom.z;
    xmax = (xmax > atom.x) ? xmax : atom.x;
    ymax = (ymax > atom.y) ? ymax : atom.y;
    zmax = (zmax > atom.z) ? zmax : atom.z;
    }
    return [[xmin, ymin, zmin], [xmax, ymax, zmax], [xsum / cnt, ysum / cnt, zsum / cnt]];
  }

  getResiduesById(atomlist, resi) {
    var ret = [];
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    if (resi.indexOf(atom.resi) !== -1) ret.push(atom.serial);
    }
    return ret;
  }

  getResidueBySS(atomlist, ss) {
    var ret = [];
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    if (ss.indexOf(atom.ss) !== -1) ret.push(atom.serial);
    }
    return ret;
  }

  getChain(atomlist, chain) {
    var ret = [], chains = {};
    chain = chain.toString(); // concat if Array
    var i, lim;
    for (i = 0, lim = chain.length; i < lim; i++) chains[chain.substr(i, 1)] = true;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    if (chains[atom.chain]) ret.push(atom.serial);
    }
    return ret;
  }

  // for HETATM only
  getNonbonded(atomlist, chain) {
    var ret = [];
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    if (atom.hetflag && atom.bonds.length === 0) ret.push(atom.serial);
    }
    return ret;
  }

  colorByAtom(atomlist, colors) {
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    var c = colors[atom.elem];
    if (c === undefined) c = this.ElementColors[atom.elem];
    if (c === undefined) c = this.defaulColor;
    atom.color = c;
    }
  }

  // MEMO: Color only CA. maybe I should add atom.cartoonColor.
  colorByStructure(atomlist, helixColor, sheetColor, colorSidechains) {
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    if (!colorSidechains && (atom.atom !== 'CA' || atom.hetflag)) continue;
    if (atom.ss[0] === 's') atom.color = sheetColor;
    else if (atom.ss[0] === 'h') atom.color = helixColor;
    }
  }

  colorByBFactor(atomlist, colorSidechains) {
    var minB = 1000, maxB = -1000;
    var i;
    var atom;
    for (i in atomlist) {
      atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;
      if (atom.hetflag) continue;
      if (colorSidechains || atom.atom === 'CA' || atom.atom === 'O3\'') {
          if (minB > atom.b) minB = atom.b;
          if (maxB < atom.b) maxB = atom.b;
      }
    }

    var mid = (maxB + minB) / 2;

    var range = (maxB - minB) / 2;
    if (range < 0.01 && range > -0.01) return;
    var atom;
    for (i in atomlist) {
      atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;
      if (atom.hetflag) continue;
      if (colorSidechains || atom.atom === 'CA' || atom.atom === 'O3\'') {
        var color = new THREE.Color(0);
        if (atom.b < mid)
        color.setHSV(0.667, (mid - atom.b) / range, 1);
        else
        color.setHSV(0, (atom.b - mid) / range, 1);
        atom.color = color.getHex();
      }
    }
  }

  colorByChain(atomlist, colorSidechains) {
    var i;
    for (i in atomlist) {
      var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;
      if (atom.hetflag) continue;
      if (colorSidechains || atom.atom === 'CA' || atom.atom === 'O3\'') {
          var color = new THREE.Color(0);
          color.setHSV((atom.chain.charCodeAt(0) * 5) % 17 / 17.0, 1, 0.9);
          atom.color = color.getHex();
      }
    }
  }

  colorByResidue(atomlist, residueColors) {
    var i;
    var c;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;
    c = residueColors[atom.resn]
    if (c !== undefined) atom.color = c;
    }
  }

  colorAtoms(atomlist, c) {
    var i;
    for (i in atomlist) {
    var atom = this.atoms[atomlist[i]]; if (atom === undefined) continue;

    atom.color = c;
    }
  }

  colorByPolarity(atomlist, polar, nonpolar) {
    var polarResidues = ['ARG', 'HIS', 'LYS', 'ASP', 'GLU', 'SER', 'THR', 'ASN', 'GLN', 'CYS'];
    var nonPolarResidues = ['GLY', 'PRO', 'ALA', 'VAL', 'LEU', 'ILE', 'MET', 'PHE', 'TYR', 'TRP'];
    var colorMap = {};
    var i;
    for (i in polarResidues) colorMap[polarResidues[i]] = polar;
    for (i in nonPolarResidues) colorMap[nonPolarResidues[i]] = nonpolar;
    this.colorByResidue(atomlist, colorMap);
  }

  drawSymmetryMates2(group, asu, matrices) {
    if (matrices === undefined) return;
    asu.matrixAutoUpdate = false;
    var i, j;
    var cnt = 1;
    this.protein.appliedMatrix = new THREE.Matrix4();
    for (i = 0; i < matrices.length; i++) {
    var mat = matrices[i];
    if (mat === undefined || mat.isIdentity()) continue;
    var symmetryMate = THREE.SceneUtils.cloneObject(asu);
    symmetryMate.matrix = mat;
    group.add(symmetryMate);
    for (j = 0; j < 16; j++) this.protein.appliedMatrix.elements[j] += mat.elements[j];
    cnt++;
    }
    this.protein.appliedMatrix.multiplyScalar(cnt);
  }

  drawSymmetryMatesWithTranslation2(group, asu, matrices) {
    if (matrices === undefined) return;
    var p = this.protein;
    asu.matrixAutoUpdate = false;
    var i, a, b, c;
    for (i = 0; i < matrices.length; i++) {
    var mat = matrices[i];
    if (mat === undefined) continue;

    for (a = -1; a <=0; a++) {
        for (b = -1; b <= 0; b++) {
          for (c = -1; c <= 0; c++) {
              var translationMat = new THREE.Matrix4().makeTranslation(
              p.ax * a + p.bx * b + p.cx * c,
              p.ay * a + p.by * b + p.cy * c,
              p.az * a + p.bz * b + p.cz * c);
              var symop = mat.clone().multiplySelf(translationMat);
              if (symop.isIdentity()) continue;
              var symmetryMate = THREE.SceneUtils.cloneObject(asu);
              symmetryMate.matrix = symop;
              group.add(symmetryMate);
          }
        }
    }
    }
  }

  defineRepresentation() {
    var all = this.getAllAtoms();
    var hetatm = this.removeSolvents(this.getHetatms(all));
    this.colorByAtom(all, {});
    this.colorByChain(all);
    this.drawAtomsAsSphere(this.modelGroup, hetatm, this.sphereRadius);
    this.drawMainchainCurve(this.modelGroup, all, this.curveWidth, 'P');
    this.drawCartoon(this.modelGroup, all, true, this.curveWidth);
  }

  setBackground(hex, a) {
    a = a | 1.0;
    this.bgColor = hex;
    //this.renderer.setClearColorHex(hex, a);
    this.scene.fog.color = new THREE.Color(hex);
  }

  zoomInto(atomlist, keepSlab) {
    var tmp = this.getExtent(atomlist);
    var center = new THREE.Vector3(tmp[2][0], tmp[2][1], tmp[2][2]);//(tmp[0][0] + tmp[1][0]) / 2, (tmp[0][1] + tmp[1][1]) / 2, (tmp[0][2] + tmp[1][2]) / 2);
    if (this.protein.appliedMatrix) {center = this.protein.appliedMatrix.multiplyVector3(center);}
    //this.modelGroup.setAttribute('position') = center.multiplyScalar(-1);
    var x = tmp[1][0] - tmp[0][0], y = tmp[1][1] - tmp[0][1], z = tmp[1][2] - tmp[0][2];

    var maxD = Math.sqrt(x * x + y * y + z * z);
    if (maxD < 25) maxD = 25;

    if (!keepSlab) {
       this.slabNear = -maxD / 1.9;
       this.slabFar = maxD / 3;
    }
    this.rotationGroup.position.z = maxD * 0.35 / Math.tan(Math.PI / 180.0 * this.camera.fov / 2) - 150;
    // this.rotationGroup.quaternion = new THREE.Quaternion(1, 0, 0, 0);
  }

  loadMoleculeString(source) {
    this.atoms = []
    this.protein = {sheet: [], helix: [], biomtChains: '', biomtMatrices: [], symMat: [], pdbID: '', title: ''}
    this.parsePDB2(source)
    // this.zoomInto(this.getAllAtoms())
    // this.show();
  }

  setSlabAndFog() {
    var center = this.rotationGroup.position.z - this.camera.position.z;
    if (center < 1) center = 1;
    this.camera.near = center + this.slabNear;
    if (this.camera.near < 1) this.camera.near = 1;
    this.camera.far = center + this.slabFar;
    if (this.camera.near + 1 > this.camera.far) this.camera.far = this.camera.near + 1;
    if (this.camera instanceof THREE.PerspectiveCamera) {
    this.camera.fov = this.fov;
    } else {
    this.camera.right = center * Math.tan(Math.PI / 180 * this.fov);
    this.camera.left = - this.camera.right;
    this.camera.top = this.camera.right / this.ASPECT;
    this.camera.bottom = - this.camera.top;
    }
    this.camera.updateProjectionMatrix();
    this.scene.fog.near = this.camera.near + this.fogStart * (this.camera.far - this.camera.near);
    // if (this.scene.fog.near > center) this.scene.fog.near = center;
    this.scene.fog.far = this.camera.far;
  }
}

export default MoleculeViewer
