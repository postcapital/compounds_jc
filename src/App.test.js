import '@testing-library/jest-dom/extend-expect'
import MoleculeViewer from './Molecule.js';
import App from './App';
import React, {Component} from 'react';
import compounds from './compounds.json';

import { 
  waitFor,
  screen,
  cleanup, 
  fireEvent, 
  render 
} from "@testing-library/react"

const MockMoleculeViewer = () => {
  React.useEffect(() => {
    console.log('using mock MoleculeViewer');
  });
  return (<div>MoleculeViewer</div>);
};
jest.mock('./Molecule', () => ({
  __esModule: true,
  namedExport: jest.fn(),
  default: jest.fn()
}));

beforeAll(() => {
  MoleculeViewer.mockImplementation(MockMoleculeViewer);
});


test("tests initial molecule_formula", () => {

  const { getByText } = render(<App />);
  const formula = document.getElementById("molecule_formula").innerHTML
  expect(formula).toMatch('C25H21N5O2');
  expect(formula).not.toMatch('C25H21N5O2 ');
  expect(formula).not.toMatch('C17H22N2O3S');

});

test("tests P R E V button changes from 0 to [98]", () => {

  const { getByText } = render(<App />);
  const button = getByText("P R E V");
  
  fireEvent.click(button);
  const formula = document.getElementById("molecule_formula").innerHTML;
  expect(formula).toMatch('C17H22N2O3S');
  
});

test("tests P R E V button changes from 0 to [97]", () => {

  const { getByText } = render(<App />);
  const button = getByText("P R E V");
  
  fireEvent.click(button);
  // twice clicked from 0
  fireEvent.click(button);
  const formula = document.getElementById("molecule_formula").innerHTML;
  expect(formula).toMatch('C19H20N2O4S');
  expect(formula).not.toMatch('C17H22N2O3S');
  
});


afterEach(cleanup);
