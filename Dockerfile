FROM python:latest
COPY build/* /
EXPOSE 8000
CMD python -m http.server 8000

