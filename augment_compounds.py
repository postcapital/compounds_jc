import json
from openbabel import pybel

cf = open('compounds.json', encoding='utf-8-sig')

compounds = json.loads(cf.read())

for n in range(len(compounds)):
  mol = pybel.readstring('smi', compounds[n]['smiles'])
  mol.make3D()
  compounds[n]['pdb'] = mol.write('pdb')

of = open('src/compounds.json', 'w')
of.write(json.dumps(compounds))


